#!/usr/bin/env sh
set -e
cd /var/www/html

CMD=$1
ARG1=$2
ARG2=$3

case "$CMD" in

    "install" )
        composer install
     ;;

    "update" )
        composer update
     ;;

esac

if [ "$ARG1" = "set_env" ]; then

	if [ ! -f .env ]; then
		cp .env.${DOCKER_ENV}.dist .env
		chmod 0777 .env
	fi

fi

if [ "$ARG2" = "migrate" ]; then

    /wait-for-it.sh database:3306 -s -t 60 -- echo "Successfully connected to database."

    until php yii migrate --interactive=0; do
        printf 'Database is seeding by some dummy data...'
        sleep 3
    done

fi

php-fpm
