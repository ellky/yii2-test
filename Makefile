.ONESHELL:

-include ./docker/.env

file = -f docker-compose.yml -f docker-compose.$(DOCKER_ENV).yml

#docker-compose = USER=$(id -u) GROUP=$(id -g) cd docker && docker-compose $(file)
docker-compose = cd docker && docker-compose $(file)

PROJECT_INFRASTRUCTURE= $(PWD)/docker
PROJECT_SRC= $(PWD)/app

USER_UID=$(shell id -u)
USER_GID=$(shell id -g)

web = php-fpm
db = db

DOCKER_COMPOSE_FILES=-f ${PROJECT_INFRASTRUCTURE}/docker-compose.${DOCKER_ENV}.yml

help:
	@echo ""
	@echo "usage: make CommandName, fe: 'make app-logs'"
	@echo ""
	@echo "------------------------------------------------------"
	@echo "-- App --"
	@echo "------------------------------------------------------"
	@echo "app-logs                 Show logs from application."
	@echo "app-build                Build application."
	@echo "app-up                   Rise up application."
	@echo "app-down                 Shot down application."
	@echo "app-start                Start application."
	@echo "app-stop                 Stop application."
	@echo "app-restart              Restart application."
	@echo "app-recreate             Recreate application."
	@echo "app-rebuild              Rebuild of application."
	@echo ""
	@echo "------------------------------------------------------"
	@echo "-- Web --"
	@echo "------------------------------------------------------"
	@echo "web-bash                 Bash inside web container."
	@echo "web-logs                 Show logs from web container."
	@echo "web-build                Build web container."
	@echo "web-up                   Rise up web container."
	@echo "web-down                 Shot down web container."
	@echo "web-start                Start web container."
	@echo "web-stop                 Stop web container."
	@echo "web-restart              Restart web container."
	@echo "web-recreate             Recreate web container."
	@echo "web-set-env              Set web environment based on defined docker environment variable."
	@echo ""
	@echo "------------------------------------------------------"
	@echo "-- Docker --"
	@echo "------------------------------------------------------"
	@echo "docker-set-env           Set docker environment, f.e: make app-set-env env=dev."

app-logs:
	@$(docker-compose) logs -f ${container}
app-build:
	@$(docker-compose) build
app-up:
	@$(docker-compose) up -d
app-down:
	@$(docker-compose) down
app-start:
	@$(docker-compose) start
app-stop:
	@$(docker-compose) stop
app-restart: app-stop app-start
app-recreate: app-down app-up
app-rebuild: app-down app-build app-up

web-bash:
	@$(docker-compose) exec $(web) /bin/bash
web-logs:
	@$(docker-compose) logs -f $(web)
web-build:
	@$(docker-compose) build $(web)
web-up:
	@$(docker-compose) up -d $(web)
web-down:
	@$(docker-compose) down $(web)
web-start:
	@$(docker-compose) start $(web)
web-stop:
	@$(docker-compose) stop $(web)
web-restart: web-stop web-start
web-recreate: web-down web-up


docker-set-env:
	@cd docker
	@if [ ! -f .env ]; then
		@cat .env.$(env).dist > .env
		echo ".env file for docker was successfully copied!"
	@else
		echo ".env file already exists!"
	@fi
