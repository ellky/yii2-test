<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => self::env('DB_DSN'),
    'username' => self::env('DB_USER'),
    'password' => self::env('DB_PASSWORD'),
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
